import React, { Component } from 'react';
import data from './data.json';
import _ from 'lodash';
window.data = data;

const books = {};
for(const o of data) {
  books[o.id] = o;
}

const boxShadow = '3px 3px 5px rgba(0,0,0,0.5)';

function header(id) {
    const o = books[id];
    return <div style={{
      display: 'inline-block',
      marginBottom: 10,
      paddingBottom: 5,
      width: '100%',
      boxShadow,
    }}>
      <img style={{
        maxWidth: window.innerWidth * .614 / 1.614 - 20,
        maxHeight: Math.min(300, window.innerHeight / 3),
        margin: '15px 10px 10px 10px',
        border: '1px solid white',
        float: 'left',
        boxShadow,
      }}
    src={`covers/${o.id}.jpg`} />
      <div style={{
        marginTop: 15,
        fontSize: 24,
        fontWeight: 'bold',
      }}>{o.title}</div>
      <div style={{
        fontSize: 18,
      }}>{o.creator}</div>
      <div>{o.tags.map(s => <span style={{
        display: 'inline-block',
        border: '1px solid #ccc',
        borderRadius: 4,
        margin: '4px 8px 4px 0px',
        padding: 4,
      }}> {s} </span>)}</div>
      </div>
}

const minThumbWidth = 130;
function recommendations(id) {
  const o = books[id];
  const cols = Math.ceil(window.innerWidth / minThumbWidth);
  const thumbWidth = (window.innerWidth - 32) / cols | 0;
  let result = []
  for(let i = 0; i < cols; ++i) {
    result.push([]);
  }
  let i = 0;
  for(const [idx, weight] of o.related) {
    result[i++ % cols].push(idx);;
  }
  for(let j = 0; j < 20; ++j) {
    result[i++ % cols].push(data[(Math.random() * data.length)|0].id);
  }
  result = result.map(arr => arr.map( idx => (
      <a href={'?'+idx}>
      <img style={{
        width: thumbWidth - 12,
        marginLeft: 6,
        marginBottom: 8,
        border: '1px solid white',
        boxShadow,
      }}
      src={`covers/${idx}.jpg`}
      title={books[idx].title}
      />
      </a>)))
  return <div style={{marginLeft: 16}}>{result.map(o => {
    return (
      <div style={{
      verticalAlign: 'top',
      display: 'inline-block',
      width: thumbWidth,
    }}>
      {o}
    </div>)
  })}
    </div>
}

function topbar() {
  const imgs = []
  for(let i=0;i< 8;++i) {
    const idx = data[(Math.random() * data.length)|0].id;
    imgs.push( 
      <img style={{
        height:60,
          margin:0,padding:0,
      }}
      src={`covers/${idx}.jpg`}
      title={books[idx].title}
      />)
  }
  return <div style={{
    height: 60,
    boxShadow
  }}>
    {imgs}
    </div>
}

class App extends Component {
  render() {
    const id = (+window.location.search.slice(1)) || data[(Math.random() * data.length)|0].id;
    return <div>
      {/*topbar()*/}
      {header(id)}
    {recommendations(id)}
      </div>
  }
}


export default App;
