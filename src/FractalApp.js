import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import data from './data.json';
import _ from 'lodash';
window.data = data;

const books = {};
for(const o of data) {
  books[o.id] = o;
}


function fractal({recurse, x,y,size}) {
  const childSize = Math.sqrt(2) - 1;
  const offset = recurse * Math.PI / 4; //Math.random() * Math.PI * 2;
  const count = 8;
  function children() {
    let result = [];
    for(let j = 0; j < count ; ++ j) {
      result = result.concat(fractal({
        recurse: recurse - 1,
        y: y + size * Math.cos(offset + j / count * Math.PI * 2),
        x: x + size * Math.sin(offset + j / count * Math.PI * 2),
        size: size * childSize,
        //i: (Math.random() * data.length)|0
        //i: o.related[Math.pow(Math.random(), 1.1 ) * 50 | 0][0]
      }))
    }
    return result;
  }
  return [{ x, y, size }].concat(recurse > 0 ? children() : [])
}

function layout(o) {
  const points = _.reverse(_.sortBy(fractal(o), 'size'));
  let result = [];
  for(const a of points) {
    let ok = true;
    for(const b of result) {
      const s = b.size / 2;
      if(b.x - s < a.x && a.x < b.x + s  &&
         b.y - s < a.y && a.y < b.y + s) {
        ok = false;
        break
      }
    }
    if(ok) {
      result.push(a);
    }
  }
  console.log(points, result);
  console.log(JSON.stringify(result));
}



const ratio = .7;
function BookInfo({recurse, x,y,size,i}) {
  const o = books[i];
  const imgSize = 1;
  const childSize = Math.sqrt(2) - 1;
  const offset = recurse * Math.PI / 4; //Math.random() * Math.PI * 2;
  //const offset = Math.random() * Math.PI * 2;
  const count = 8;
  function children() {
    const result = [];
    for(let j = 0; j < count ; ++ j) {
      result.push(BookInfo({
        recurse: recurse - 1,
        y: y + size * Math.cos(offset + j / count * Math.PI * 2),
        x: x + size * Math.sin(offset + j / count * Math.PI * 2) * ratio,
        size: size * childSize,
        //i: (Math.random() * data.length)|0
        i: o.related[Math.pow(Math.random(), 1.1 ) * 50 | 0][0]
      }))
    }
    return result;
  }
  return <div style={{}}>
    <img style={{position: 'absolute', 
        display: 'inline-block',
        zIndex: recurse,
        top: y - .5 * size * imgSize, 
        left: x - .5 * size * imgSize * ratio,
        height: size * imgSize - 4,
        width: size * imgSize * ratio - 4,
        background: '#ccc',
        border: '1px solid #111',
    }} 
  title={`${o.title}\n${o.creator}`}
  src={`covers/${o.id}.jpg`}
    />
    {recurse > 0 && children()}
  {/*o.related.map(([i, _]) =>
      <img style={{height:100, margin: 0, padding: 0}}
      src={`covers/${books[i].id}.jpg`} />
      )*/}
    </div>
}

const scale = Math.sqrt(2) - 1;

function subFractal(recurse) {
  if(recurse < 1) {
    return [];
  }

  const subSize = (1 - scale * scale) / 2;
  return [[recurse, 0.5, scale * scale]].concat(
    subFractal(recurse - 1).map(([r, pos, size]) => [r, pos * subSize, subSize * size])
  ).concat(
    subFractal(recurse - 1).map(([r, pos, size]) => [r, 1 - pos * subSize, subSize * size])
  );
}

let count = 0;
function Fractal({x, y, size, recurse, sides, color}) {
  sides = sides | (sides >> 4)
  let children = [];


  // 1 2
  //  x 
  // 8 4

  let p = (1+scale) / 2;
  const corners = [[-p, p], [p,p], [p,-p], [-p, -p]];
  for(let i = 0; i < 4; ++i) {
    if(recurse >= 1 && ((1 << i) & sides)) {
      const [dx, dy] = corners[i];
      let newSides = 0x11 << i;
      newSides = newSides | (newSides << 1) | (newSides << 2)
      newSides = newSides >> 1;
      children.push(Fractal({
        x: x + dx * size,
        y: y + dy * size,
        size: size * scale,
        recurse: recurse - 1,
        sides: newSides,
        color
      }));
    }
  }


  //  1
  // 4x2
  //  3
  const sideInfo = [
    [0, 1],
    [1, 0],
    [0, -1],
    [-1, 0],
  ];
  const sideSides = [3,6, 12, 9];


  if(recurse > 0) {
    for(let i = 0; i < 4; ++i) {
      const [dx, dy] = sideInfo[i];
      children = children.concat(subFractal(recurse - 1).map(([r, pos, sz]) =>
        Fractal({
          x: x + dx * size * (.5 + sz/2) + size * (1 - (i&1)) * (pos - 0.5),
          y: y + dy * size * (.5 + sz/2) + size * (i&1) * (pos - 0.5),
          size: size * sz,
          recurse: r - 1,
          sides: sideSides[i],
          color
        })
      ))
    }
  }

  /*
  children = children.concat(subFractal(recurse - 1).map(([r, pos, sz]) =>
    Fractal({
      x: x + size * (-0.5 + pos),
      y: y + size * (0.5 + sz/2),
      size: size * sz,
      recurse: r - 1,
      sides: 3
    })
  ))
  */

  console.log(++count);
  return <div>
    <img style={{
      display: 'inline-block',
      position: 'absolute',
        top: y - size/2, left: (x - size / 2) * ratio,
        width: size * ratio,
        height: size,
        background: 'white', //color,
        border: '1px black'
    }}
    />
    {children}
    </div>
}

function InitialFractal(x,y,size, r, color) {
  return <div>
    {Fractal({ recurse: r + 1, x, y, size, sides: 15, color})}
    </div>
    /*
  {Fractal({ recurse: r, x: x + size, y, size: size * scale, sides: 15})}
  {Fractal({ recurse: r, x: x - size, y, size: size * scale, sides: 15})}
  {Fractal({ recurse: r, x, y: y + size, size: size * scale, sides: 15})}
  {Fractal({ recurse: r, x, y: y - size, size: size * scale, sides: 15})}
  */
}

const a = Math.sqrt(2) - 1

class App extends Component {
  render() {
    layout({ recurse: 3, x: 0, y: 0, size: 1});
    const r = 2;
    const divs = []
    for(let i = 0; i < 5; ++i) {
      for(let j = -1; j < 4; ++j) {
        divs.push(
        InitialFractal(170*i-70*j,70*i+170*j,100, r, 'red')
        );
      }
    }
    return <div>{divs}</div>
  }
}




export default App;
