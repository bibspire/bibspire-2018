import React, { Component } from 'react';
import logo from './logo.svg';
import data from './data.json';
import _ from 'lodash';
import fractal from './fractal.json';
window.data = data;

const books = {};
for(const o of data) {
    books[o.id] = o;
}

const ratio = 0.7

function recommendations(i) {
  // 1 8 44 216
  let recs = [i];
  recs = recs.concat(books[i].related.map(([i,w]) => i));
  for(let j = 0; j < 1+8+44+216; ++j) {
    recs.push(data[(Math.random() * data.length)|0].id)
  }
  return recs;
}

function BookInfo({x,y,size,i}) {
  const [x0, y0, sz] = [x,y, size]
  const result = [];
  const recs = recommendations(i);

  i = 0;
  for(const {x, y, size} of fractal /*.slice(0,53)*/) {
    const o = books[recs[i++]];
    const s = size * sz;
    const ss = s / 10;
    result.push(
      <a href={'?' + o.id}>
    <img style={{position: 'absolute', 
        display: 'inline-block',
        left: x0 + (x * sz - .5 * s) * ratio - 3,
        top: y0 + y * sz - .5 * s - 3,
        width: s  * ratio - 6,
        height: s - 6,
        background: '#ccc',
        boxShadow: `${ss/3}px ${ss/3}px ${ss}px rgba(0,0,0,0.8)`,
        border: `1px solid white`,
    }} 
  title={`${o.title}\n${o.creator}`}
  src={`covers/${o.id}.jpg`}
    /></a>)
  }
  return result;
}

function f() {
  const sizes = {};
  for(const {size} of fractal) {
    sizes[size] = (sizes[size]||0) + 1;
  }
  console.log(fractal.length, sizes);
}


class App extends Component {
  render() {
    const i = (+window.location.search.slice(1)) || data[(Math.random() * data.length)|0].id;
    const o = books[i];
    const landscape = window.innerWidth > window.innerHeight;
    const sideWidth = 320;
    return (
      <div>
      {landscape?
        <div style={{
        position: 'absolute',
        display: 'inline-block',
        width: sideWidth - 20,
        zIndex: 100,
        top: 0,
        left: 0,
        bottom: 0,
        margin: 0,
        padding: 10,
        background: '#fcfffa',
        textAlign: 'center',
        boxShadow: '0 0 15px rgba(0,0,0,0.8)'
        }}>
      <h1>{o.title.replace(/:.*/, '')}</h1>
      <h2>af {o.creator}</h2>
      <div>
      {o.tags.map(s => [<span>{s}</span>, ' '])}
      </div>
      <p/>
      <img style={{maxWidth: sideWidth - 20}} src={`covers/${o.id}.jpg`} />
      </div>
        : <div style={{
        textAlign: 'center',
            marginTop: 5,
            fontSize: 14
        }}>
      <div style={{fontSize: 20, fontWeight: 'bold'}}>{o.title.replace(/:.*/, '')}</div>
      <em>af {o.creator}</em> 
        <div>
      {o.tags.map(s => [<span>{s}</span>, ' '])}
        </div>
        </div>}
      {BookInfo({ 
        recurse: 3, 
        x: landscape 
          ? sideWidth + (window.innerWidth - sideWidth) / 2
          : window.innerWidth / 2,
        y: window.innerHeight / 2 + (landscape ? 0 : 40),
        size: Math.min(window.innerWidth/ 2.2 - (landscape? sideWidth/2: 0), 
          window.innerHeight / 3.2 - (landscape ? 0 : 20)),
        i}) }
      </div>
    );
  }
}


export default App;
